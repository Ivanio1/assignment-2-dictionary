%include "lib.inc"
global find_word

section .text

find_word:

	.loop:
		push rdi            ;сохраним caller-saved регистр
		push rsi            ;сохраним caller-saved регистр
		add rsi, 8          ;переходим к метке
		call string_equals  ;сравниваем значения и сохраняем результат в rax
		pop rsi
		pop rdi
		cmp rax, 1             
		je .found
		mov rsi, [rsi]      ;<--Если словарь пройден и слово не нашлось, то переходим к not_found
		cmp rsi, 0          ;--------------------------------------------------------------------
		je .not_found       ;------------------------------------------------------------------->
		jmp .loop

	.found:             ;Если вхождение найдено
		mov rax, rsi    ;адрес начала вхождения->rax
		ret

	.not_found:         ;Если вхождение не найдено
		xor rax, rax    ;0->rax
		ret
