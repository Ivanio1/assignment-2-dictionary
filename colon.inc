%macro colon 2

	%ifid %2
		%ifdef POINTER
			%%previous: dq POINTER		; Локальная метка previous
		%else
			%%previous: dq 0
		%endif
		%define POINTER %%previous		; В POINTER всегда адрес начала списка
	%else
		%fatal 'Второй элемент не ID'
	%endif

	%ifstr %1
		db %1, 0					; Запись ключа(0-нуль терминированная строка)
		%2:
	%else
		%fatal 'Первый элемент не строка'
	%endif

%endmacro
