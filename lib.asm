%define EXIT 60
%define    READ    0
%define    WRITE   1
%define STDERR 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


section .data
nl_char: db 10 ; символ перехода на след. строку

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, EXIT          ;номер системного вызова exit
    syscall                    ;системный вызов




; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rcx - счетчик
; нуль-термированная - это оканчивающаяся на 0x00
; rdi- указатель на нуль-терминированную строку
string_length:
    xor rcx, rcx                ;сброс rcx
     mov rax, -1
.loop:
    inc rax
    test byte [rdi+rax], 0xFF   ;Сравниваем с 0
    jne .loop
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - input string
; rax - системный вызов
; rdi - stdout
; rsi - строка
; rdx - длинна
print_string:
	push    rdi             ;сохраним caller-saved регистр
    call    string_length   ;string_length -> rax
    pop     rsi             ;кладём адрес начала строки-источника(rdi) в rsi 
    mov rdx, rax            ;кладём длину строки в rdx
    mov rax, WRITE          ;номер системного вызова write
    mov rdi, 1              ;дескриптор stdout
    syscall
    ret


; Принимает код символа и выводит его в stdout
; rdi - символ печати
print_char:
    push rdi        ;Сохраняем значение rdi в стек
    mov rax,WRITE      ;номер системного вызова write
    mov rdi, 1      ;дескриптор stdout
    mov rsi, rsp    ;кладём адрес начала строки-источника в rsi
    mov rdx, 1      ;длина символа в rdx
    syscall
    pop rdi         ;Возвращаем значение со стека в rdi
    ret


printer:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, STDERR
    mov rax, 1
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
   mov rdi, 0xA
   call print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - число
print_uint:
    mov rax, rdi         ; Записываем число в rax
    mov r8, 0x0A         ; Записываем делитель(10)
    mov r8, 10
    push 0x00            ; Записываем в стек нуль-терминатор
    .loop:               ; Накапливаем остатки в стеке
        xor rdx, rdx     ;Обнуляем rdx
        div r8           ; rax - частное, rdx - остаток
        add rdx, 0x30    ; Получем ASCII код числа
        push rdx         ; Сохраняем в стек
        cmp rax, r8      ; Сравниваем частное с 10, если больше или равно - делим еще раз
        jae .loop
    add rax, 0x30        ; Получем ASCII код числа
    cmp rax, 0x30        ; Сравниваем с 0, если равно-переходим к следующему
    je .next
    push rax

    .next:               ; Снимаем остатки со стека
        pop rdi
        cmp rdi, 0x00
        je .eof
        call print_char
        jmp .next
    .eof:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - число
print_int:
    mov r8, rdi
    test rdi, rdi        ;Проверяем знаковое ли число
    jns .print           ;Если беззнаковое, то переходим к выводу беззнакового
    mov rdi, '-'         ;rdi='-'
    call print_char      ;Печатаем минус
    mov rdi, r8
    neg rdi              ;Переводим число в дополнительный код-изменяем знак числа
    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - первый
; rsi - второй
string_equals:
    xor rcx, rcx     ;Очищаем счётчик
    .loop:
        mov al, byte[rdi+rcx]   ;Сохраняем символ первой строки
        mov dl, byte[rsi+rcx]    ;Сохраняем символ второй строки
        cmp al,dl               ;Сравниваем символа
        jne .neqls               ;Если не равны, то Возвращаем 0
        inc rcx                 ;Увеличиваем счётчик
        cmp al, 0x00            ;Сравниваем символ первой строки с 0
        je .eqls                ;Если следующий символ=нуль-терминатор, то возвращаем 1
        jmp .loop               ;Иначе переходим к сравнению следующих символов
    .eqls:
        mov rax, 1
        ret
    .neqls:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0          ;кладём 0 на стек
    mov rax, READ     ;номер системного вызова
    mov rdi, 0       ;дескриптор stdin
    mov rsi, rsp    ;Записываем адрес символа
    mov rdx, 1      ;Записыаем кол-во символов для чтения
    syscall
    pop rax  ;Обнуляем rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; rdi - начало буфера
; rsi - размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0x10.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера. 
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    dec rsi
    mov r9, rsi
    xor r10, r10
.validate:
  	call read_char
  	cmp rax, 0x20
  	je .validate
  	cmp rax, 0x9
  	je .validate
  	cmp rax, 0xA
  	je .validate
.next_char:
  	cmp rax, 0
  	jz .success
  	cmp rax, 0x20
  	je .success
  	cmp rax, 0x9
  	je .success
  	cmp rax, 0xA
  	je .success
  	cmp r10, r9
  	je .error
  	mov [r8 + r10], rax
  	inc r10
  	call read_char
  	jmp .next_char
.success:
  	mov [r8 + r10], byte 0
  	mov rax, r8
  	mov rdx, r10
  	ret
.error:
  	xor rax, rax
  	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor r8, r8
    mov r10, 0x0A
    mov r10, 10
    .num:               
        cmp byte[rdi+rcx], 0x30     ; <--30<=symbol<=39 (ASCII 0-9 numbers)
        jb .not_correct             ; --
        cmp byte[rdi+rcx], 0x39     ; -->
        ja .not_correct
        mul r10                     ; <--sum*10 + num
        mov r8b, byte[rdi+rcx]      ; -->
        sub r8b, 0x30               ;Возвращаем символ
        add rax, r8                 ;Сохраняем число
        inc rcx
        jmp .num
    .not_correct:
        mov rdx, rcx                ;Сохраняем длину
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'      ;Сравниваем первый символ строки с минусом
    jne parse_uint          ;Если не минус, то печатаем
    inc rdi                 ;Переходим к следующему символу
    call parse_uint         ;Читаем символ
    neg rax                 ;Переводим число в дополнительный код-изменяем знак числа
    inc rdx                 ;Увеличиваем длину
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длинна буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rdx
    call string_length          ; Определение длины строки
    cmp rdx, rax                ;Если строка не умещается в буфер, возвращаем  0
    jle .carry
    .loop:
        xor rcx, rcx
        mov cl, byte[rdi]        ; Запись текущего элемента строки
        mov byte[rsi], cl       ; Запись в буфер текущего элемента строки
        inc rdi
        inc rsi
        test rcx, rcx    
        jnz .loop
    pop rdx
    pop rdi
    ret
    .carry:
        pop rdx
        pop rdi
        mov rax, 0
        ret
