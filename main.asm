%include "lib.inc"
%include "words.inc"

global _start

extern find_word

section .rodata
	msg_too_long: db 'The entered string is too long.', 0
	msg_not_found: db 'Key not found!', 0

%define BUFFER_SIZE 256
section .bss
input_buffer: resb BUFFER_SIZE 

section .text

_start:
	mov rdi, input_buffer					;<--Считывание слова с stdin
	mov rsi, BUFFER_SIZE			;---------------------------
	call read_word				;-------------------------->
	cmp rax, 0					;<--Если значение слишком большое для буфера, то выводим нужное сообщение
	jz .too_long_err				;----------------------------------------------------------------------->

	mov rdi, rsp				;Ищем значение в словаре, если найдено-выводим, если нет выводим нужное сообщение
	mov rsi, POINTER
	call find_word
	cmp rax, 0
	jz .not_found_err
	
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax

	mov rdi, rax
	call print_string
	xor rdi,rdi
    call exit

	.too_long_err:
		mov rdi, msg_too_long
		jmp .error

	.not_found_err:
		mov rdi, msg_not_found
		jmp .error

	.error:
		call printer
		mov rdi,1
		call exit
